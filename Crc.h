#ifndef CRC_CALCULATE
#define CRC_CALCULATE

#include "vtypes.h"

class CCrc
{
	uint16_t polinom;
	int flag;
	uint16_t crc;

public:
	CCrc(){ Clear(); polinom=0xa001; }
	CCrc(uint16_t polinom)
	{
		this->polinom=polinom;
		Clear();
	}

   uint16_t Get(){ return crc; }

	void Clear(){ crc=0xffff; }
	void NextItem(uint8_t item)
	{
		crc^=item;
		for(int j=0; j<8; j++)
		{
			flag=crc&0x0001;
			crc>>=1;
			if(1==flag) crc^=polinom;
		}
	}
	void SomeNextItems(const uint8_t *items, uint16_t count)
	{
		for(uint16_t i=0; i<count; i++) NextItem(items[i]);
	}
	void SomeNextItems(const uint8_t *items, uint16_t start, uint16_t count)
	{
		for(uint16_t i=0; i<count; i++) NextItem(items[start+i]);
	}
};

#endif
