#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QByteArray>
#include <QSerialPort>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QSerialPort port;

    void addLogLine(const QString &line);
    bool aduTransaction(uint8_t addr, const QByteArray &pduReq, QByteArray &pduAnsv, int timeout);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void transactionSlot(void);
    void on_readArchiveButton_released();

private:
    Ui::MainWindow *ui;

signals:
    void transactionSignal(void);
};

#endif // MAINWINDOW_H
