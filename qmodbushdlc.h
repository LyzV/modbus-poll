#ifndef QMODBUSHDLC_H
#define QMODBUSHDLC_H

#include <QByteArray>
#include "vtypes.h"

#define PDU_MAXSIZE      ((uint8_t)0xF5)
#define ADU_PREFIX_SIZE  ((uint8_t)1)
#define ADU_SUFFIX_SIZE  ((uint8_t)2)
#define ADU_MAXSIZE      ((uint8_t)(ADU_PREFIX_SIZE+PDU_MAXSIZE+ADU_SUFFIX_SIZE))

class IModbusHDLC
{
public:
    virtual void clear(void)=0;
    virtual bool pdu2adu(uint8_t addr, const QByteArray &pdu, QByteArray &adu) const =0;
    virtual bool received(uint8_t addr, const QByteArray &chunk, QByteArray &pdu)=0;
};

class QModbusHDLC: public IModbusHDLC
{
    QByteArray array;

    bool checkCRC(const uint8_t *adu, uint8_t aduSize) const;

public:
    QModbusHDLC();

    virtual void clear(void);
    virtual bool pdu2adu(uint8_t addr, const QByteArray &pdu, QByteArray &adu) const;
    virtual bool received(uint8_t addr, const QByteArray &chunk, QByteArray &pdu);
};

#endif // QMODBUSHDLC_H
