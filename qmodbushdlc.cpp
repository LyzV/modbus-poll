#include "qmodbushdlc.h"
#include "Crc.h"

bool QModbusHDLC::checkCRC(const uint8_t *adu, uint8_t aduSize) const
{
    CCrc crcCalc;
    crcCalc.SomeNextItems(adu, aduSize-2);

    uint16_t crc16;
    crc16 =          adu[aduSize-2];
    crc16+=(uint16_t)adu[aduSize-1]<<8;

    if(crc16!=crcCalc.Get())
        return false;

    return true;
}

QModbusHDLC::QModbusHDLC()
{
    clear();
}

void QModbusHDLC::clear(void)
{
    array.clear();
}

bool QModbusHDLC::pdu2adu(uint8_t addr, const QByteArray &pdu, QByteArray &adu) const
{
    if(PDU_MAXSIZE < pdu.count()) return false;

    adu.clear();
    adu.append(addr);
    adu.append(pdu);
    CCrc crcCalc;
    crcCalc.SomeNextItems((const uint8_t *)adu.constData(), adu.count());
    adu.append((uint8_t)(crcCalc.Get()   ));
    adu.append((uint8_t)(crcCalc.Get()>>8));
    return true;
}

bool QModbusHDLC::received(uint8_t addr, const QByteArray &chunk, QByteArray &pdu)
{
    //�������� �� ADU
    array.append(chunk);
    //��������
    uint32_t count=array.count();
    if((ADU_PREFIX_SIZE+ADU_SUFFIX_SIZE) > count)
        return false;//���������� �������� ADU
    if((ADU_MAXSIZE) < count)
    {
        array.clear();//����� �� ����
        return false;
    }
    if(addr!=(uint8_t)array.at(0))
    {
        array.clear();//����� �� ����
        return false;
    }
    if(false==checkCRC((const uint8_t *)array.constData(), count))
        return false;//���������� �������� ADU

    //�������� PDU
    pdu.clear();
    pdu=array.mid(ADU_PREFIX_SIZE, count-(ADU_PREFIX_SIZE+ADU_SUFFIX_SIZE));

    array.clear();//��� ����� ADU
    return true;
}
