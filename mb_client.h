#ifndef MB_CLIENT_H
#define MB_CLIENT_H

#include "vtypes.h"
#include <QByteArray>
#include "modbus.h"


/*
class IModbusHDLC
{
public:
    virtual void clear(void)=0;
    virtual bool pdu2adu(uint8_t addr, const QByteArray &pdu, QByteArray &adu) const =0;
    virtual bool received(uint8_t addr, const QByteArray &chunk, QByteArray &pdu)=0;
};
*/

class ModbusClientParser
{
public:

    //������ ������� ��� ���������� ������� �� ������ �������� ������
    void buildSubReq20(uint16_t fileNumber,
                       uint16_t recordNumber,
                       uint16_t recordLength,
                       modbus::request20_t::SubReq &subReq) const;
    void addSubReq20(const modbus::request20_t::SubReq &subReq, modbus::request20_t &pduReq) const;

    bool  request20(const modbus::request20_t &pduReq, QByteArray &pdu) const;
    bool response20(const QByteArray &pdu, modbus::response20_t &pduAnsv, uint8_t &exception) const;


    //������ ������� ��� ���������� ������� �� ������ �������� ������
    void buildSubReq21(uint16_t fileNumber,
                       uint16_t recordNumber,
                       const QVector<uint16_t> &recordData,
                       modbus::request21_t::SubReq &subReq) const;
    void addSubReq21(const modbus::request21_t::SubReq &subReq, modbus::request21_t &pduReq) const;


    bool  request21(const modbus::request21_t &pduReq, QByteArray &pdu) const;
    bool response21(const QByteArray &pdu, modbus::response21_t &pduAnsv, uint8_t &exception) const;


    //������ ������� ��� ������ �������� ������������� Func43 MEI14
    void buildReq43Mei14(uint8_t readDeviceID, uint8_t objectID, modbus::request43mei14_t &req) const;

    void  request43mei14(modbus::request43mei14_t &req, QByteArray &pdu) const;
    bool response43mei14(const QByteArray &pdu, modbus::response43mei14_t &pduAnsv, uint8_t &exception) const;

};

#endif // MB_CLIENT_H
