#ifndef MODBUS_HPP
#define MODBUS_HPP

#include "vtypes.h"
#include <QVector>

namespace modbus
{

/*
    �������
*/
#define READ_COILS                  ((uint8_t)1)	// ������ �����
#define READ_INPUT_DISCRETE         ((uint8_t)2)	// ������ ���������� ������
#define READ_HOLDING_REGISTERS      ((uint8_t)3)	// ������ ��������� ���������
#define READ_INPUT_REGISTERS		((uint8_t)4)	// ������ ������� ���������
#define WRITE_SINGLE_COIL			((uint8_t)5)	// ������ ����
#define WRITE_SINGLE_REGISTER		((uint8_t)6)	// ������ ��������
#define WRITE_MULTIPLE_COILS		((uint8_t)15)	// ������ ��������� �����
#define WRITE_MULTIPLE_REGISTERS	((uint8_t)16)	// ������ ��������� ���������
#define READ_FILE_RECORDS           ((uint8_t)20)	// ������ �������� �������
#define WRITE_FILE_RECORDS          ((uint8_t)21)	// ������ �������� �������
#define MODBUS_DEVICE_INCAPSULATION ((uint8_t)43)	// ������ ����������������� ����������

/*
    ���� ����������
*/
#define MB_ILLEGAL_FUNCTION			((uint8_t)1)	// ������ �������
#define MB_ILLEGAL_DATA_ADDRESS		((uint8_t)2)	// ������ ������
#define MB_ILLEGAL_DATA_VALUE		((uint8_t)3)	// ������ ������
#define MB_SERVER_DEVICE_FAILURE	((uint8_t)4)	// ������ ����������

struct request3_t
{
   uint16_t StartingAddress;//Starting Address
   uint16_t QntyRegisters;//Quantity of Registers
};

struct request4_t
{
   uint16_t StartingAddress;//Starting Address
   uint16_t QntyRegisters;//Quantity of Input Registers
};

struct request5_t
{
   uint16_t OutputAddress;//Output Address
   uint16_t OutputValue;//Output Value
};

struct respose5_t
{
   uint16_t   OutputAddress;//Output Address
   uint16_t   OutputValue;//Output Value
};

struct request6_t
{
   uint16_t RegisterAddress;//Register Address
   uint16_t RegisterValue;//Register Value
};

struct response6_t
{
   uint16_t   RegisterAddress;//Register Address
   uint16_t   RegisterValue;//Register Value
};

struct request16_t
{
    void clear(void)
    {
        StartingAddress=0;
        RegisterValues.clear();
    }
    void copy(const request16_t &req)
    {
        StartingAddress=req.StartingAddress;
        RegisterValues.clear();
        RegisterValues.append(req.RegisterValues);
    }
    uint16_t StartingAddress;//Starting Address
    QVector<uint16_t> RegisterValues;//Registers Value

    request16_t(){ clear(); }
    request16_t(const request16_t &req){ copy(req); }
    request16_t &operator =(const request16_t &req){ copy(req); return *this; }
};

struct response16_t
{
   uint16_t   StartingAddress;//Starting Address
   uint16_t   QntyRegisters;//Quantity of Registers
};


struct request20_t//������ �� ������ �����
{
    uint8_t byteCount;//Byte Count
    struct SubReq
    {
        uint8_t refType;//Reference Type
        uint16_t fileNumber;//File Number
        uint16_t recordNumber;//Record Number
        uint16_t recordLength;//Record Length
    };
    QVector<SubReq> subReqList;//������ �����������
};

struct response20_t
{
    uint8_t dataLength;//Resp. data Length
    struct SubAnsv
    {
        uint8_t fileRespLength;//File Resp. length
        uint8_t refType;//Reference Type
        QVector<uint16_t> recordData;//Record Data
    };
    QVector<SubAnsv> subAnsvList;//������ ����������
};

struct request21_t//������ �� ������ �����
{
    uint8_t dataLength;//Request data length
    struct SubReq
    {
        uint8_t refType;//Reference Type
        uint16_t fileNumber;//File Number
        uint16_t recordNumber;//Record Number
        uint16_t recordLength;//Record length
        QVector<uint16_t> recordData;//Record data
    };
    QVector<SubReq> subReqList;//������ �����������
};

struct response21_t
{
    uint8_t dataLength;//Response Data length
    struct SubAnsv
    {
        uint8_t refType;//Reference Type
        uint16_t fileNumber;//File Number
        uint16_t recordNumber;//Record Number
        uint16_t recordLength;//Record length
        QVector<uint16_t> recordData;//Record data
    };
    QVector<SubAnsv> subAnsvList;//������ ����������
};

struct request43mei14_t
{
    uint8_t meiType;//MEI Type
    uint8_t readDeviceID;//Read Device ID code
    uint8_t objectID;//Object Id
};

struct response43mei14_t
{
    uint8_t meiType;//MEI Type
    uint8_t readDeviceID;//Read Device ID code
    uint8_t confLevel;//Conformity Level
    uint8_t moreFollows;//More Follows
    uint8_t nextObjectID;//Next Object Id
    struct ObjectT
    {
        uint8_t ID;//Object ID
        QByteArray value;//Object value
    };
    QVector<ObjectT> objectList;//������ ��������
};

} // namespace modbus

#endif
