#include "mb_client.h"
#include "modbus.h"

using namespace modbus;

//������� �����������
#define SUB_REQ20_SIZE          (sizeof(request20_t::SubReq::refType)+ \
                                 sizeof(request20_t::SubReq::fileNumber)+ \
                                 sizeof(request20_t::SubReq::recordNumber)+ \
                                 sizeof(request20_t::SubReq::recordLength))
#define SUB_REQ21_SIZE(len)     (sizeof(request21_t::SubReq::refType)+ \
                                 sizeof(request21_t::SubReq::fileNumber)+ \
                                 sizeof(request21_t::SubReq::recordNumber)+ \
                                 sizeof(request21_t::SubReq::recordLength)+(len<<1))
#define SUB_ANSV21_SIZE(len)    SUB_REQ21_SIZE(len)

/*
������:
+---------------------------+---------+--------------------+
| Function code             | 1 Byte  | 0x14               |
+---------------------------+---------+--------------------+
| Byte Count                | 1 Byte  | 0x07 to 0xF5 bytes | ����� ����� ������� � ������
+---------------------------+---------+--------------------+ 							  <-\
| Sub-Req.x, Reference Type | 1 Byte  | 06                 | ��������� 6  					 |
+---------------------------+---------+--------------------+    		        			 |
| Sub-Req.x, File Number    | 2 Bytes | 0x0001 to 0xFFFF   | ����� �����					 |
+---------------------------+---------+--------------------+     							  > ���������
| Sub-Req.x, Record Number  | 2 Bytes | 0x0000 to 0x270F   | ����� ���������� ��������		 |
+---------------------------+---------+--------------------+    							 |
| Sub-Req.x, Record Length  | 2 Bytes | N                  | ���-�� ���������				 |
+---------------------------+---------+--------------------+ 							  <-/
| Sub-Req.x+1, ...          |         |                    |
+---------------------------+---------+--------------------+
*/

void ModbusClientParser::buildSubReq20(uint16_t fileNumber,
                                       uint16_t recordNumber,
                                       uint16_t recordLength,
                                       request20_t::SubReq &subReq) const
{
    subReq.fileNumber=fileNumber;
    subReq.refType=6;
    subReq.recordNumber=recordNumber;
    subReq.recordLength=recordLength;
}

void ModbusClientParser::addSubReq20(const request20_t::SubReq &subReq, request20_t &pduReq) const
{
    pduReq.subReqList.append(subReq);
    pduReq.byteCount+=SUB_REQ20_SIZE;
}

bool ModbusClientParser::request20(const request20_t &pduReq, QByteArray &pdu) const
{
    pdu.clear();
    pdu.append(READ_FILE_RECORDS);
    if((uint8_t)(pduReq.subReqList.count()*SUB_REQ20_SIZE) != pduReq.byteCount) return false;
    pdu.append(pduReq.byteCount);
    for(uint8_t i=0; i<pduReq.subReqList.count(); ++i)
    {
        request20_t::SubReq subReq=pduReq.subReqList.at(i);
        if(6!=subReq.refType) return false;
        pdu.append(6);//Reference Type
        pdu.append(subReq.fileNumber>>8);//File Number Hi
        pdu.append(subReq.fileNumber);//File Number Lo
        pdu.append(subReq.recordNumber>>8);//Record Number Hi
        pdu.append(subReq.recordNumber);//Redcord Number Lo
        pdu.append(subReq.recordLength>>8);//Record Length Hi
        pdu.append(subReq.recordLength);//Redcord Length Lo
    }
    return true;
}

/*
�����:
+-----------------------------+-----------+--------------------+
| Function code               | 1 Byte    | 0x14               |
+-----------------------------+-----------+--------------------+
| Resp. data Length           | 1 Byte    | 0x07 to 0xF5       | ����� ����� ������ � ������
+-----------------------------+-----------+--------------------+                         <-\
| Sub-Req.x, File Resp.length | 1 Byte    | 0x07 to 0xF5       | ����� ���������            |
+-----------------------------+-----------+--------------------+                            |
| Sub-Req.x, Reference Type   | 1 Byte    | 6                  | ��������� 6                 > ��������
+-----------------------------+-----------+--------------------+                            |
| Sub-Req.x, Records Data     | N*2 Bytes |                    | N ���������                |
+-----------------------------+-----------+--------------------+                         <-/
| Sub-Req.x+1, ...            |           |                    |
+-----------------------------+-----------+--------------------+
*/
bool ModbusClientParser::response20(const QByteArray &pdu, response20_t &pduAnsv, uint8_t &exception) const
{
    exception=0;
    uint8_t count=pdu.count();
    if(2>count)
    {
        exception=MB_ILLEGAL_DATA_ADDRESS;
        return false;
    }
    //Function Code
    if(READ_FILE_RECORDS!=pdu.at(0))
    {
        if((0x80|READ_FILE_RECORDS)!=(uint8_t)pdu.at(0))
        {//�� ��������� �������
            exception=MB_ILLEGAL_FUNCTION;
            return false;
        }
        //����� �����������
        exception=pdu.at(1);//��� ����������
        return true;
    }
    //Resp. data Length
    pduAnsv.dataLength=pdu.at(1);
    if((7>pduAnsv.dataLength)||(0xF5<pduAnsv.dataLength))
    {//�� ���������� ���� - �� ������������� �����
        exception=MB_ILLEGAL_DATA_ADDRESS;
        return false;
    }
    if((count-2)!=pduAnsv.dataLength)
    {//�� ���������� ���� - �� ������������� �����
        exception=MB_ILLEGAL_DATA_ADDRESS;
        return false;
    }
    if(0x1 & pduAnsv.dataLength)
    {//�� ���������� ���� - respDataLength ������ ���� ������
        exception=MB_ILLEGAL_DATA_ADDRESS;
        return false;
    }

    pduAnsv.subAnsvList.clear();
    response20_t::SubAnsv subAnsv;
    for(//i - ������� � pdu, ����������� �� ������ ���������� ���������
        uint8_t i=2;//�������� �� ������ ���� ������� ���������
        i<count;
        i+=sizeof(response20_t::SubAnsv::fileRespLength)+subAnsv.fileRespLength//�� ��������� ��������
       )
    {
        //Sub-Req.x, File Resp.length
        subAnsv.fileRespLength=pdu.at(i);
        if((7>subAnsv.fileRespLength)||(0xF5<subAnsv.fileRespLength))
        {//�� ���������� ���� - �� ������������� �����
            exception=MB_ILLEGAL_DATA_ADDRESS;
            return false;
        }
        if(i+sizeof(response20_t::SubAnsv::fileRespLength)+subAnsv.fileRespLength < count)
        {//�� ������ ���� ����� fileRespLength
            exception=MB_ILLEGAL_DATA_ADDRESS;
            return false;
        }
        if(0==(0x1 & subAnsv.fileRespLength))
        {//������ ���� ��������
            exception=MB_ILLEGAL_DATA_ADDRESS;
            return false;
        }

        //Sub-Req.x, Reference Type
        subAnsv.refType=pdu.at(i+sizeof(response20_t::SubAnsv::fileRespLength));
        if(6!=subAnsv.refType)
        {//�� ���������� reference type
            exception=MB_ILLEGAL_DATA_ADDRESS;
            return false;
        }
        //Sub-Req.x, Records Data
        uint8_t recordQnty=(subAnsv.fileRespLength-1)>>1;
        uint8_t recordDataOffset=   i+
                                    sizeof(response20_t::SubAnsv::fileRespLength)+
                                    sizeof(response20_t::SubAnsv::refType);
        subAnsv.recordData.clear();
        for(uint8_t j=0; j<recordQnty; ++j)
        {//�� ���� �������
            uint16_t value;
            uint8_t recordPosition=j<<1;
            value =(uint16_t)pdu.at(recordDataOffset+recordPosition)<<8;//Hi
            value+=pdu.at(recordDataOffset+recordPosition+1);//Lo
            subAnsv.recordData.append(value);
        }
        pduAnsv.subAnsvList.append(subAnsv);
    }

    return true;
}

void ModbusClientParser::buildSubReq21(uint16_t fileNumber, uint16_t recordNumber, const QVector<uint16_t> &recordData, request21_t::SubReq &subReq) const
{
    subReq.fileNumber=fileNumber;
    subReq.refType=6;
    subReq.recordNumber=recordNumber;
    subReq.recordLength=recordData.count();
    subReq.recordData.clear();
    subReq.recordData.append(recordData);
}

void ModbusClientParser::addSubReq21(const request21_t::SubReq &subReq, request21_t &pduReq) const
{
    pduReq.subReqList.append(subReq);
    pduReq.dataLength+=SUB_REQ21_SIZE(subReq.recordData.count());
}


/*
������:
+-----------------------------+-------------+---------------+
| Field Name                  | Sizeof      | (Hex)         |
|                             |             |               |
+-----------------------------+-------------+---------------+
| Function code               | 1 Byte      | 15            | ��������� 0x15
+-----------------------------+-------------+---------------+
| Request Data Length         | 1 Byte      | 09 to FB      | ����� ������� � ������ ������� �� ���������� ����
+-----------------------------+-------------+---------------+
| Sub-Req.x, Reference Type   | 1 Byte      | 06            | ��������� 6
+-----------------------------+-------------+---------------+
| Sub-Req.x, File Number      | 2 Bytes     | 0001 to FFFF  | ����� �����
+-----------------------------+-------------+---------------+
| Sub-Req.x, Record Number    | 2 Bytes     | 0000 to 270F  | ����� ���������� ��������
+-----------------------------+-------------+---------------+
| Sub-Req.x, Record Length    | 2 Bytes     | N             | ���-�� ��������� � ����������
+-----------------------------+-------------+---------------+
| Sub-Req.x, Record Data      | N * 2 Bytes |               | ������ ��������� �� 2 ����� �� �������
+-----------------------------+-------------+---------------+
| Sub-Req.x+1, ...            |             |               |
+-----------------------------+-------------+---------------+
*/
bool ModbusClientParser::request21(const request21_t &pduReq, QByteArray &pdu) const
{
    pdu.clear();
    pdu.append(WRITE_FILE_RECORDS);
    uint8_t requestDataLength=0;
    pdu.append(pduReq.dataLength);//Request Data Length
    for(uint8_t i=0; i<pduReq.subReqList.count(); ++i)
    {
        request21_t::SubReq subReq=pduReq.subReqList.at(i);
        if(6!=subReq.refType) return false;
        pdu.append(6);//Reference Type
        pdu.append(subReq.fileNumber>>8);//File Number Hi
        pdu.append(subReq.fileNumber);//File Number Lo
        pdu.append(subReq.recordNumber>>8);//Record Number Hi
        pdu.append(subReq.recordNumber);//Redcord Number Lo
        pdu.append(subReq.recordLength>>8);//Record Length Hi
        pdu.append(subReq.recordLength);//Redcord Length Lo
        if(subReq.recordLength!=subReq.recordData.count()) return false;
        for(uint16_t j=0; j<subReq.recordData.count(); ++j)//Record Data
        {
            uint16_t record=subReq.recordData.at(j);
            pdu.append(       record>>8);
            pdu.append(0xff & record);
        }
        requestDataLength+=SUB_REQ21_SIZE(subReq.recordLength);
    }
    if(requestDataLength != pduReq.dataLength) return false;

    return true;
}

/*
�����:
+-----------------------------+-------------+---------------+
| Field Name                  | Sizeof      | (Hex)         |
|                             |             |               |
+-----------------------------+-------------+---------------+
| Function code               | 1 Byte      | 15            |
+-----------------------------+-------------+---------------+
| Response Data Length        | 1 Byte      | 09 to FB      |
+-----------------------------+-------------+---------------+
| Sub-Ansv.x, Reference Type  | 1 Byte      | 06            |
+-----------------------------+-------------+---------------+
| Sub-Ansv.x, File Number     | 2 Bytes     | 0001 to FFFF  | ����� �����
+-----------------------------+-------------+---------------+
| Sub-Ansv.x, Record Number   | 2 Bytes     | 0000 to 270F  | ����� ���������� ��������
+-----------------------------+-------------+---------------+
| Sub-Ansv.x, Record Length   | 2 Bytes     | N             | ���-�� ��������� � ���������
+-----------------------------+-------------+---------------+
| Sub-Ansv.x, Record Data     | N * 2 Bytes |               | ������ ��������� �� 2 ����� �� �������
+-----------------------------+-------------+---------------+
| Sub-Ansv.x+1, ...           |             |               |
+-----------------------------+-------------+---------------+
*/
bool ModbusClientParser::response21(const QByteArray &pdu, response21_t &pduAnsv, uint8_t &exception) const
{
    exception=0;
    uint8_t count=pdu.count();
    if(2>count)
    {
        exception=MB_ILLEGAL_DATA_ADDRESS;
        return false;
    }
    //Function Code
    if(WRITE_FILE_RECORDS!=pdu.at(0))
    {
        if((0x80|WRITE_FILE_RECORDS)!=(uint8_t)pdu.at(0))
        {//�� ��������� �������
            exception=MB_ILLEGAL_FUNCTION;
            return false;
        }
        //������� ����������
        exception=pdu.at(1);//��� ����������
        return true;
    }
    //Resp. data Length
    pduAnsv.dataLength=pdu.at(1);
    if((9>pduAnsv.dataLength)||(0xFB<pduAnsv.dataLength))
    {//�� ���������� ���� - �� ������������� �����
        exception=MB_ILLEGAL_DATA_ADDRESS;
        return false;
    }
    if(0 == (0x1 & pduAnsv.dataLength))
    {//�� ���������� ���� - respDataLength ������ ���� �� ������
        exception=MB_ILLEGAL_DATA_ADDRESS;
        return false;
    }

    //Parse SubAnsv-s
    pduAnsv.subAnsvList.clear();
    response21_t::SubAnsv subAnsv;
    uint8_t respDataLength=0;//������������ ����� ����� �����������
    uint8_t subAnsvLength;//����� � ������ ���������
    for(//i - ������� � pdu, ����������� �� ������ ���������� ���������
        uint8_t i=2;//�������� �� ������ ���� ������� ���������
        i<count;
        i+=subAnsvLength//�� ��������� ��������
       )
    {
        //Sub-Req.x, Reference Type
        subAnsv.refType=pdu.at(i);
        if(6!=subAnsv.refType)
        {//�� ���������� reference type
            exception=MB_ILLEGAL_DATA_ADDRESS;
            return false;
        }
        //Sub-Ansv.x, File Number
        subAnsv.fileNumber = (uint16_t)pdu.at(i+sizeof(subAnsv.refType)  )<<8;//Hi
        subAnsv.fileNumber+=           pdu.at(i+sizeof(subAnsv.refType)+1);//Lo
        //Sub-Ansv.x, Record Number
        subAnsv.recordNumber = (uint16_t)pdu.at(i+sizeof(subAnsv.refType)+sizeof(subAnsv.fileNumber)  )<<8;//Hi
        subAnsv.recordNumber+=           pdu.at(i+sizeof(subAnsv.refType)+sizeof(subAnsv.fileNumber)+1);//Lo
        //Sub-Ansv.x, Record Length
        subAnsv.recordLength = (uint16_t)pdu.at(i+sizeof(subAnsv.refType)+sizeof(subAnsv.fileNumber)+sizeof(subAnsv.recordNumber)  )<<8;//Hi
        subAnsv.recordLength+=           pdu.at(i+sizeof(subAnsv.refType)+sizeof(subAnsv.fileNumber)+sizeof(subAnsv.recordNumber)+1);//Lo
        if(0==subAnsv.recordLength)
        {//�� ���������� ���� - ������ ���� ����-�� ���� ������
            exception=MB_ILLEGAL_DATA_ADDRESS;
            return false;
        }
        //����� � ������ ������� ���������
        subAnsvLength=SUB_ANSV21_SIZE(subAnsv.recordLength);
        if(i+subAnsvLength < count)
        {//�� ������ ���� ����� recordLength
            exception=MB_ILLEGAL_DATA_ADDRESS;
            return false;
        }
        //��������� ����� ����� ������. ����� ��������...
        respDataLength+=subAnsvLength;

        //Sub-Ansv.x, Records Data
        uint8_t recordDataOffset=   i+ //�������� � pdu, ��� ����������� ������ ���������
                                    sizeof(response21_t::SubAnsv::refType)+
                                    sizeof(response21_t::SubAnsv::fileNumber)+
                                    sizeof(response21_t::SubAnsv::recordNumber)+
                                    sizeof(response21_t::SubAnsv::recordLength);
        subAnsv.recordData.clear();
        for(uint8_t j=0; j<subAnsv.recordLength; ++j)
        {//�� ���� �������
            uint16_t value;
            uint8_t recordPosition=j<<1;
            value =(uint16_t)pdu.at(recordDataOffset+recordPosition)<<8;//Hi
            value+=pdu.at(recordDataOffset+recordPosition+1);//Lo
            subAnsv.recordData.append(value);//��������� ��������� ������
        }
        //��������� ��������
        pduAnsv.subAnsvList.append(subAnsv);
    }
    //��� ������ ��������� ����� ����� ������
    if(respDataLength!=pduAnsv.dataLength)
    {//�� ���������� ���� - �� ������������� �����
        exception=MB_ILLEGAL_DATA_ADDRESS;
        return false;
    }

    return true;
}

/*
������:
+----------------------+--------+-------------------+
| Function code        | 1 Byte | 0x2B              | ������ 0x2B (43)
+----------------------+--------+-------------------+
| MEI Type*            | 1 Byte | 0x0E              | ������ 0x0E (14)
+----------------------+--------+-------------------+
| Read Device ID code  | 1 Byte | 01 / 02 / 03 / 04 | ��� ������� � ��������: ���������; ��������������.
| description level    |        |                   |
+----------------------+--------+-------------------+
| Object Id            | 1 Byte | 0x00 to 0xFF      | ����� (ID) ������� ������� ���������
+----------------------+--------+-------------------+
*/
void ModbusClientParser::buildReq43Mei14(uint8_t readDeviceID, uint8_t objectID, request43mei14_t &req) const
{
    req.meiType=14;
    req.readDeviceID=readDeviceID;
    req.objectID=objectID;
}

void ModbusClientParser::request43mei14(request43mei14_t &req, QByteArray &pdu) const
{
    pdu.clear();
    pdu.append(MODBUS_DEVICE_INCAPSULATION);
    pdu.append(req.meiType);
    pdu.append(req.readDeviceID);
    pdu.append(req.objectID);
}
/*
�����:
+----------------------+--------+----------------------------+
| Function code        | 1 Byte | 0x2B                       | ������ 43 (0x2B)
+----------------------+--------+----------------------------+
| MEI Type             | 1 Byte | 0x0E                       | ������ 14 (0x0E)
+----------------------+--------+----------------------------+
| Read Device ID code  | 1 Byte | 01 / 02 / 03 / 04          | ����� ���� "Read Device ID code" �� �������
+----------------------+--------+----------------------------+
| Conformity level     | 1 Byte | 01 / 02 / 03 / 04 or       | ������������� ������ ������� "Read Device ID code"
|                      |        | 0x81 or 0x82 or 0x83       |
+----------------------+--------+----------------------------+
| More Follows         | 1 Byte | 00 / FF                    | 0 - ���� ����������, FF - ����� ��� ����������
+----------------------+--------+----------------------------+
| Next Object Id       | 1 Byte | Object ID number           | � �������� ���������� � ����� ������ ����� ���������� �����
+----------------------+--------+----------------------------+
| Number of objects    | 1 Byte |                            | ����� ������� � ������ ��������
+----------------------+--------+----------------------------+
| List Of              |        |                            |
+----------------------+--------+----------------------------+
|      Object ID       | 1 Byte |                            | ����� �������
+----------------------+--------+----------------------------+
|      Object length   | 1 Byte |                            | ����� ������� � ������
+----------------------+--------+----------------------------+
|      Object Value    | length | Depending on the object ID | ���������� �������
+----------------------+--------+----------------------------+
*/
bool ModbusClientParser::response43mei14(const QByteArray &pdu, response43mei14_t &pduAnsv, uint8_t &exception) const
{
    exception=0;
    uint8_t pos=0;

    uint8_t count=pdu.count();
    if(2>count)
    {
        exception=MB_ILLEGAL_DATA_ADDRESS;
        return false;
    }
    //Function Code
    uint8_t funcCode=pdu.at(pos++);//0
    if(MODBUS_DEVICE_INCAPSULATION!=funcCode)
    {
        if((0x80|MODBUS_DEVICE_INCAPSULATION)!=funcCode)
        {//�� ��������� �������
            exception=MB_ILLEGAL_FUNCTION;
            return false;
        }
        //������� ����������
        exception=pdu.at(1);//��� ����������
        return true;
    }
    //MEI Type
    pduAnsv.meiType=pdu.at(pos++);//1
    if(14!=pduAnsv.meiType)
    {
        exception=MB_ILLEGAL_FUNCTION;
        return false;
    }
    //������� 7 ����
    uint8_t headerSize= sizeof(funcCode)+
                        sizeof(response43mei14_t::meiType)+
                        sizeof(response43mei14_t::readDeviceID)+
                        sizeof(response43mei14_t::confLevel)+
                        sizeof(response43mei14_t::moreFollows)+
                        sizeof(response43mei14_t::nextObjectID)+
                        sizeof(uint8_t);//Number of objects
    if(headerSize>count)
    {
        exception=MB_ILLEGAL_DATA_ADDRESS;
        return false;
    }
    //Read Device ID code
    pduAnsv.readDeviceID=pdu.at(pos++);//2
    switch(pduAnsv.readDeviceID)
    {
    default:
        {
            exception=MB_ILLEGAL_DATA_VALUE;
            return false;
        }
        //break;
    case 1: case 2: case 3: case 4: break;
    }
    //Conformity level
    pduAnsv.confLevel=pdu.at(pos++);//3
    switch(pduAnsv.confLevel)
    {
    default:
        {
            exception=MB_ILLEGAL_DATA_VALUE;
            return false;
        }
        //break;
    case 1: case 2: case 3: case 0x81: case 0x82: case 0x83: break;
    }
    //More Follows
    pduAnsv.moreFollows=pdu.at(pos++);//4
    switch(pduAnsv.moreFollows)
    {
    default:
        {
            exception=MB_ILLEGAL_DATA_VALUE;
            return false;
        }
        //break;
    case 0: case 0xFF: break;
    }
    //Next Object Id
    pduAnsv.nextObjectID=pdu.at(pos++);//5
    //Number of objects
    uint8_t numberOfObjects=pdu.at(pos++);//6

    //List Of...
    pduAnsv.objectList.clear();
    for(uint8_t i=0; i<numberOfObjects; ++i)
    {
        uint8_t length= pos+//��������� + ���������� �������
                        sizeof(response43mei14_t::ObjectT::ID)+//Object ID
                        sizeof(uint8_t);//Object length
        if(length>count)
        {
            exception=MB_ILLEGAL_DATA_ADDRESS;
            return false;
        }
        //Object ID
        response43mei14_t::ObjectT object;
        object.ID=pdu.at(pos++);
        //Object length
        uint8_t objectLength=pdu.at(pos++);
        if((pos+objectLength)>count)
        {
            exception=MB_ILLEGAL_DATA_ADDRESS;
            return false;
        }
        //Object Value
        object.value=pdu.mid(pos, objectLength); pos+=objectLength;
        pduAnsv.objectList.append(object);
    }
    if(pos!=count)
    {//������� ����� ���� � pdu!
        exception=MB_ILLEGAL_DATA_ADDRESS;
        return false;
    }
    return true;
}
