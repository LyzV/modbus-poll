#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "vtypes.h"
#include "modbus.h"
#include "mb_client.h"
#include "qmodbushdlc.h"
#include <QThread>
#include <QApplication>
#include <QTime>

using namespace modbus;

void MainWindow::addLogLine(const QString &line)
{
    ui->logEdit->append(line);
}

bool MainWindow::aduTransaction(uint8_t addr, const QByteArray &pduReq, QByteArray &pduAnsv, int timeout)
{
    QModbusHDLC hdlc;
    QByteArray aduReq;
    QByteArray rcvChunk;
    QTime time;

    this->thread()->msleep(3);//����� ����� �������
    time.start();//��������� timeout

    if(false==hdlc.pdu2adu(addr, pduReq, aduReq))
    {
        addLogLine("Error: PDU failue.");
        return false;
    }
    pduAnsv.clear();
    hdlc.clear();

    if(false==port.open(QIODevice::ReadWrite))
    {
        addLogLine("Error: Can't open serial port.");
        return false;
    }
    addLogLine("Info: Serial port opened successfully.");

    int timeElapsed=time.elapsed();
    port.clear();
    port.write(aduReq);
    if(false==port.waitForBytesWritten(timeElapsed))
    {
        addLogLine("Error: Send timeout expired.");
        port.close();
        addLogLine("Info: Serial port closed.");
        return false;
    }
    for(
        timeElapsed=time.elapsed();
        timeElapsed<timeout;
        timeElapsed=time.elapsed()
       )
    {
        if(false==port.waitForReadyRead(0))
        {
            emit transactionSignal();
            continue;
        }
        rcvChunk=port.readAll();
        if(true==hdlc.received(addr, rcvChunk, pduAnsv))
        {
            port.close();
            addLogLine("Info: Serial port closed.");
            return true;
        }
    }

    addLogLine("Error: Receive timeout expired.");
    port.close();
    addLogLine("Info: Serial port closed.");
    return false;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    port("/dev/ttyUSB3"),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    port.setBreakEnabled(false);
    port.setDataTerminalReady(false);
    port.setTextModeEnabled(false);

    port.setDataBits(QSerialPort::Data8);
    port.setBaudRate(QSerialPort::Baud9600);
    port.setParity(QSerialPort::NoParity);
    port.setStopBits(QSerialPort::OneStop);
    port.setFlowControl(QSerialPort::NoFlowControl);
    port.setBreakEnabled(false);
    port.setTextModeEnabled(false);

    connect(this, SIGNAL(transactionSignal()), this, SLOT(transactionSlot()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::transactionSlot()
{
    QApplication::processEvents();
}

void MainWindow::on_readArchiveButton_released()
{
    uint8_t deviceAddress=1;
    int receiveTimeout=1000;//1 �������

    ui->logEdit->setText("");
    ui->readArchiveButton->setEnabled(false);

    ModbusClientParser modbusClient;
    request43mei14_t req43mei14;
    response43mei14_t ansv43mei14;
    uint8_t exception;

    modbusClient.buildReq43Mei14(3, 0x80, req43mei14);

    QByteArray aduReqArray, pduReqArray;
    QByteArray aduAnsvArray, pduAnsvArray;

    QTime time;
    for(time.start(); 10000>time.elapsed();)
    {
        QApplication::processEvents();

        //1) ����� MEI-�������� 0x80-0x84
        modbusClient.request43mei14(req43mei14, pduReqArray);
        //��������� ��������� �������
        if(false==aduTransaction(deviceAddress, pduReqArray, pduAnsvArray, receiveTimeout))
        {
            addLogLine("Error: MEI Response frame error/timeout.");
            break;
        }
        //������ �����
        if(false==modbusClient.response43mei14(pduAnsvArray, ansv43mei14, exception))
        {
            addLogLine("Error: MEI response failue.");
            break;
        }
        //����� �����������?
        if(0!=exception)
        {
            addLogLine("Error: MEI response exception.");
            break;
        }
        //��������� ���������� ������
        if(5!=ansv43mei14.objectList.count())
        {
            addLogLine("Error: MEI response objects too few.");
            break;
        }
        response43mei14_t::ObjectT meiObject0x82=ansv43mei14.objectList.at(2);
        response43mei14_t::ObjectT meiObject0x83=ansv43mei14.objectList.at(3);
        response43mei14_t::ObjectT meiObject0x84=ansv43mei14.objectList.at(4);
        if(
           (2!=meiObject0x82.value.count())||
           (2!=meiObject0x83.value.count())||
           (2!=meiObject0x84.value.count())
          )
        {
            addLogLine("Error: MEI object failue.");
            break;
        }

        QString msg;
        //����� ������� �����
        uint16_t firstFileNumber=meiObject0x82.value.at(0);
        firstFileNumber+=(uint16_t)meiObject0x82.value.at(1)<<8;
        msg="Info: Object #0x82 = "; msg+=QString::number(firstFileNumber); addLogLine(msg);

        //���-�� ������� � �����
        uint16_t registerQnty=meiObject0x83.value.at(0);
        registerQnty+=(uint16_t)meiObject0x83.value.at(1)<<8;
        msg="Info: Object #0x83 = "; msg+=QString::number(registerQnty); addLogLine(msg);

        //���-�� ������ � ������
        uint16_t fileQnty=meiObject0x84.value.at(0);
        fileQnty+=(uint16_t)meiObject0x84.value.at(1)<<8;
        msg="Info: Object #0x84 = "; msg+=QString::number(fileQnty); addLogLine(msg);

        if(0==fileQnty)
        {//����� ���� �� �����
            addLogLine("Info: Archive is NOT ready!\n");
            QThread::msleep(100);
            continue;
        }


        addLogLine("Info: Archive is ready!");

        //2) ������ ������


        break;
    }
//_exit:
    ui->readArchiveButton->setEnabled(true);
}
